## Prüfungsvorleistung: Bewertungskriterien 
• zentral: Benutzbarkeit und Lerneffekt
◦ Pflichtkritierium: Beleg und Präsentation müssen direkt miteinander
verknüpft sein
• Qualitätsanforderungen:
◦ Nachvollziehbarkeit (Dokumentation als Markdown, Grafiken als PlantUML, o. ä.)
◦ Quellcode-Struktur
◦ Konzeption und Architektur
◦ Qualitätssicherung (mind. Unit Tests)
◦ Robustheit
◦ ...
◦ aber nicht grafisches Design
• valides Deployment als Docker-Container (nachvollziehbar, lauffähig)
• Veröffentlicht in HTWK-GitLab oder öffentlichem GitLab/GitHub Repository
(bevorzugt)
• gemeinsam bewertet: Präsentation, Beleg und Diskussionen


![[Pasted image 20221026201527.png]]

### Belegübersicht
![[2022ws_HTWK_Software_Engineering_andreas_both_kapitel_00-Belegarbeiten 1.pdf]]




***



## Aufgabe 1: Vertiefung von Git-Wissen

Überprüfen Sie Ihr (erworbenes) Basis-Wissens aus der letzten Übungsserie mittels den Selbsttests, die in [https://www.w3schools.com/git/default.asp?remote=gitlab](https://www.w3schools.com/git/default.asp?remote=gitlab) eingebettet sind.

Vertiefen Sie Ihr des Wissens zu den folgenden Themen. Die dargestellten Links dienen nur zum Einstieg, es existieren viele weitere Online-Quellen zu diesen sehr grundlegenden Themen:

-   Merging vs. Rebasing: [https://www.atlassian.com/de/git/tutorials/merging-vs-rebasing](https://www.atlassian.com/de/git/tutorials/merging-vs-rebasing)
-   Resetting, Checking Out & Reverting: [https://www.atlassian.com/de/git/tutorials/resetting-checking-out-and-reverting](https://www.atlassian.com/de/git/tutorials/resetting-checking-out-and-reverting)
-   Die Datei .gitignore: [https://www.atlassian.com/de/git/tutorials/saving-changes/gitignore](https://www.atlassian.com/de/git/tutorials/saving-changes/gitignore)
-   Git Cherry-Pick: [https://www.atlassian.com/de/git/tutorials/cherry-pick](https://www.atlassian.com/de/git/tutorials/cherry-pick)
-   Advanced Git log: [https://www.atlassian.com/de/git/tutorials/git-log](https://www.atlassian.com/de/git/tutorials/git-log)

### Kontrollfragen
Beantworten Sie folgende Fragen zur Überprüfung Ihres Wissensstandes:
-   Was muss getan werden, wenn ein Konflikt in einer Datei auftritt, weil verschiedene Personen gleichzeitig Änderungen vorgenommen haben? Wie wird ein solcher Konflikt bezüglich einer Quellcode-Datei angezeigt bzw. wie ist er erkennbar? Wie wird ein solcher Konflikt bezüglich einer Binärdatei (z. B. Bild-Datei) angezeigt bzw. wie ist er erkennbar?
-   Wie können lokale Änderungen zwischenge speichert werden, weil neue Aktualisierungen vom Repository eingefügt werden müssen?
-   Wann sollte der Befehl git rebase nicht eingesetzt werden?
-   Wie kann verhindert werden, dass (versehentlich) temporäre Dateien (z. B. aus Eclipse, oder von Visual Studio Code) in dem Repository abgelegt werden?
-   Was ist zu tun, um aus mehreren Entwicklungszweigen (Branches) ein gemeinsames Release zu erzeugen? Beschreiben Sie die Schritte so konkret wie möglich.
-   Wie können Sie ermitteln, wer die letzten 5 Änderungen an einer Datei blatt2.txt (aus der letzten Übungsserie) durchgeführt hat?


## Aufgabe 2: Basiswissen Docker 

Eine sehr weit verbreitete Methode, um auslieferungsfähige Container zu etablieren, die alle Abhängigkeiten einer Anwendung benötigen, ist die Docker-Technologie. Auch in diesem Modul werden die Projektergebnisse in Form von Docker Images abgegeben werden.

Bearbeiten Sie einen der beiden folgenden Docker-Grundkurse (es existieren auch viele weitere Online-Quellen zu diesem sehr grundlegenden Thema):

-   [https://entwickler.de/docker/grundkurs-docker-eine-praktische-einfuhrung-in-die-welt-der-container](https://entwickler.de/docker/grundkurs-docker-eine-praktische-einfuhrung-in-die-welt-der-container) oder 
-   [https://lerneprogrammieren.de/docker/](https://lerneprogrammieren.de/docker/) 

Vertiefen Sie Ihr Docker-Wissen entsprechend dem Befehl docker-compose: 

-   [https://docs.docker.com/compose/gettingstarted/](https://docs.docker.com/compose/gettingstarted/) 
-   [https://docs.microsoft.com/de-de/visualstudio/docker/tutorials/tutorial-multi-container-app-mysql](https://docs.microsoft.com/de-de/visualstudio/docker/tutorials/tutorial-multi-container-app-mysql) oder [https://www.baeldung.com/ops/docker-compose](https://www.baeldung.com/ops/docker-compose) 

Optional: Nutzen Sie den Dockerhub Playground, für weitere interaktive Übungen: [https://www.docker.com/101-tutorial/](https://www.docker.com/101-tutorial/) 

### Kontrollfragen
-   Wie unterscheidet sich der Docker-Ansatz von virtuellen Maschinen?
-   Was ist der Unterschied zwischen einem Docker Image und einem Docker Container?
-   Auf welche Art können Docker Containern beim Start konfiguriert werden?
-   Wie muss eine Datei docker-compose.yml konfiguriert werden, um zwei Webservices zu starten?
