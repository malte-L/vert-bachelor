### Motivation und Aufgabenstellung
- online assesments immer größere rolle (siehe letzte zwei jahre)
- offline lösung -> desktop -> flutter (wenn desktop dann könnte man auch direkt flutter testen)
- aufbauen/erweitern auf vorherige arbeit
- Aufgabenstellung: (siehe title): prototypisch editor für qti3 assesments. mit den zusatz gebrauch von der cross-plattform Möglichkeite von flutter machen; und dabei zwei verschiedene designs zu verwenden/erproben mit dem ziel alternativen zu dem design von etwa Onxy zu erproben
- noch nicht viele qti 3 compliant software da standard noch sehr neu (das dann später im talk: <-> arbeit gibt empfehlung für export nach qti 2 (kann auch andersherum verstanden werden: was muss passieren um qti 3 zu)unterstützen)




Jedoch vlt ganz kurz wie ich auf das Thema aufmerksam geworden bin: ich habe mich bei Herr Müller nach einem möglichen Thema für eine Bachelorarbeit erkundigt und wurde auf eine Bachelorverteidigung aufmerksam gemacht, -> hab dort zugehört und mich entschieden auf dem Thema aufzubauen.

Aufgabenstellung: (siehe title): prototypisch editor für qti3 assesments. mit den zusatz gebrauch von der cross-plattform Möglichkeite von flutter machen; **später:** und dabei zwei verschiedene designs zu verwenden/erproben mit dem ziel alternativen zu dem design von etwa Onxy zu erproben

**sinn?** Die Motivation für dieses Thema ist vielseitig, so werden auch viele verschiedene Aspekte mit dieser Arbeit erprobt.

Thema ist im E-Learning -> größte Motivation: die letzten zwei Jahre mit der Coronapandemie **und** generell das E-Learning in ein oder der anderen Form immer größere Rolle spielen wird.

Eine weitere Motivation ist, dass der QTI3 Standard noch relativ neu ist (erstens/zweites quatal dieses Jahres) und damit gibt noch nicht viele Softwaresysteme die gebrauch von dieser Version des Standards machen. D.h. in der Arbeit gehe ich auch auf unterschiede zwischen QTI3 und 2 ein und was diese für eine Implementierung eines rückwärtskompatiblen QTI3 System bedeuten.




### Einordnung in den Kontext (z.B. Ausgangssituation, Grundlagen, Vorarbeiten)

- qti(3) erklären

damit dann vorherige arbeit erklären:

- delivery-system für qti3: was heißt das?: anzeigen von einzelnen aufgaben; durchlaufen eines kompletten Tests


#### QTI3
Der QTI Standard soll den Austausch von Daten aus Assessments zwischen Organisationen zu erleichtern. Dafür beschreibt der Standard ein Datenmodel zur Beschreibung von einzelnen Fragen, Bewertungen, Ergebnissen, sowie dem grundsätzlichen Aufbau eines kompletten Tests; als Grundlage dient dafür XML. 

Vorteil. Beispiel Onyx im Opal. Ausgabe dieser Anwendung kann in Onyx dann importiert werden.

Die für diese Arbeit wichtigste Datenstruktur ist das Item; es enthält Informationen über Fragestellungen, Antwortmöglichkeiten (Interaktionen), sowie Bewertung und Feeback einer Aufgabe.

```xml
<?xml version="1.0" encoding="UTF-8"?>

<qti-assessment-item xmlns="http://www.imsglobal.org/xsd/imsqtiasi_v3p0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

xsi:schemaLocation="http://www.imsglobal.org/xsd/imsqtiasi_v3p0 https://purl.imsglobal.org/spec/qti/v3p0/schema/xsd/imsqti_asiv3p0_v1p0.xsd"

identifier="choiceMultiple" title="Composition of Water" adaptive="false" time-dependent="false">

<qti-response-declaration identifier="RESPONSE" cardinality="multiple" base-type="identifier">

<qti-correct-response>

<qti-value>H</qti-value>

<qti-value>O</qti-value>

</qti-correct-response>

<qti-mapping lower-bound="0" upper-bound="2" default-value="-2">

<qti-map-entry map-key="H" mapped-value="1" />

<qti-map-entry map-key="O" mapped-value="1" />


</qti-mapping>

</qti-response-declaration>

<qti-outcome-declaration identifier="SCORE" cardinality="single" base-type="float" />

<qti-item-body>

<qti-choice-interaction response-identifier="RESPONSE" shuffle="true" max-choices="0">

<qti-prompt>Which of the following elements are used to form water?</qti-prompt>

<qti-simple-choice identifier="H" fixed="false">Hydrogen</qti-simple-choice>

<qti-simple-choice identifier="He" fixed="false">Helium</qti-simple-choice>

<qti-simple-choice identifier="C" fixed="false">Carbon</qti-simple-choice>

<qti-simple-choice identifier="O" fixed="false">Oxygen</qti-simple-choice>

<qti-simple-choice identifier="N" fixed="false">Nitrogen</qti-simple-choice>

<qti-simple-choice identifier="Cl" fixed="false">Chlorine</qti-simple-choice>

</qti-choice-interaction>

</qti-item-body>

<qti-response-processing template="https://www.imsglobal.org/question/qti_v3p0/rptemplates/map_response.xml" template-location="../rptemplates/map_response.xml" />

</qti-assessment-item>
```

#### Flutter
Flutter ist ein Software Developement Kit (**SDK**), mit welchem **cross-plattform** Anwen-
dungen entwickelt werden können.

Zuerst: **IOS** und **Android**, jetzt aber auch **desktop** 

#### vorherige Arbeit
Meine Arbeit baut auf eine vorherige Bachelorarbeit auf welche Flutter für eine Delivery System für QTI Assesments benutzt hat. 
XML -> zu Ansichten
In dieser Arbeit Ansichten -> XML 


### Erfolgskriterien 
für drei simple aufgabentypen
- [ ] Export der Daten dieser Views 
- [ ] views für Erstellen der Interaktionen
- [ ] Export gesamten Test (ZIP-Archiv)
	- bündeln von assets

**interaktionstypen benennen**
- drag and drop mit spezialfall bilder -> textpassagen

***
12 minuten
***

Vorgehensweise an Erfolgskriterien und Design-Architektur orientiert.

Hier wurde MVVM als Architektur gewählt, um die entwicklung der grafischen oberfläche von der entwicklung der buisness logic (oder back-end) zu separieren.

MVVM besteht aus drei logischen schichten:

Die **model** schicht ist die datenebene und enthält auch buisness logic.
Hier können daten objekte z.B. lokal gespeichert werden, oder mit einem web-server synchronisiert werden.

Wie auch in anderen Architekturen dient die **view** dazu das daten**modell** dem für den nutzer optisch aufzubereiten.
Der Nutzer kann mit der view **interagieren**. 
**Interaktionen** werden von der view an das **viewmodel** weitergeleitet.

Das **viewmodel** ist eine **abstraktion** der **view** und stellt aufbereitete eigenschaften der datenebene und weitere befehle für die view bereit.
Eine view bindet an ein **viewmodel** wordurch die kommunikation zwischen den beiden schichten automatisiert wird.
Das erstellen dieser bindings erfordet in anderen frameworks viel boilerplate, kann in flutter aber mit nahezu gar keinem code ausgedrückt werden, weshalb diese architektur auch gewählt wurde.
***
2 minuten
***


Als erstes wurde die datenebene umgesetzt.
Dazu wurde der implementierungs guide von ims direkt benutzt um datenobjekte des qti standards, dazu gehört einzelne interaktionstypen, ein item, eine sektion oder ein kompletter test in dart klassen zu übersetzten.
Der implementirungsguide gibt aufschluss darüber, in welche objekte der code unterteilt werden sollte und welche eigenschaften diese objekte haben sollten.
Darauf aufbauend wurde der **export**  einzelner aufgaben umgesetzt und eprobt, um das 1. erfolgskriterium abhacken zu können und die buisness logic implementiert zu haben.


Dann wurde die view und die dazugehörigen viewmodel implementiert.
Vorerst für adwaita und gemäß der erfolgskriterien von innen nach aussen, d.h. erst intaktion, dann item, dann sektion und dann (vollständige anwendung) test.


Um das letzte erfolgskriterium abhaken zu können wurde dann der export eines kompletten test implementiert; hier werden alle einzelnen aufgaben einzeln exportiert und zusammem mit assets in einem zip-archiv gebündelt.
Assets sind in diesem prototyp die bilder, welche bei der drag-and-drop-interaktion zugeordnet werden.
Ausserdem wird noch eine datei angelegt, welche den kompletten test in seiner struktur beschreibt und auf einzelne aufgaben verweist.


Als letztes wurde dann die anwendung noch für android speziell entwickelt, d.h. hier wurde eine andere, für android passende, designsprache verwendet, um die selben inhalte und funktionen dem nutzer auf tablets bereitzustellen.
Diese UI sieht zwar optisch anders aus, hat jedoch diesselbe strukur wie die adwaita oberfläche, d.h. es gibt eine view für jede interaktion, für aufgaben, für sektionen und ganze tests.
Die ähnliche struktur erlaubt es diesselben viewmodel wiederzuverwenden, für android wurde also nur UI code erneut geschrieben.
***
4 minuten
***


